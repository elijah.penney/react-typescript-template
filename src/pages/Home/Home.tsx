import React from 'react';
import { Helmet } from 'react-helmet';

const Home: React.FC = () => {
  return (
    <div>
      <Helmet>
        <title>Home</title>
      </Helmet>
      Home Page
    </div>
  );
};

export default Home;
