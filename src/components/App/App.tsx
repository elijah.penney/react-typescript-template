import React from 'react';
import { Router } from '@components';
import { GlobalStyle, Theme } from './styles';

export const App: React.FC = () => {
  return (
    <>
      <GlobalStyle />
      <Theme>
        <Router />
      </Theme>
    </>
  );
};
