import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html {
    margin: 0;
    padding: 0;
    font-size: 16px;
    height: 100%;
    width: 100%;
  }

  body {
    margin: 0;
    padding: 0;
    font-size: 16px;
    color: rgba(0, 0, 0, 0.7);
    height: 100%;
    width: 100%;
  }
`;
