import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from '@pages/Home';

export const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Route component={Home} />
    </BrowserRouter>
  );
};
