const { merge } = require('webpack-merge');
const common = require('./webpack.common');
const CompressionPlugin = require('compression-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = merge(common, {
  mode: 'production',
  devtool: false,
  optimization: {
    minimize: true,
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin({
        async: false,
        typescript: {
          memoryLimit: 4096
        }
    }),
    new CompressionPlugin({
      filename: '[path][name].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|jsx|ts|tsx|css)$/,
    }),
  ]
});
